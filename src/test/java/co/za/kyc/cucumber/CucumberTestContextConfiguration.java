package co.za.kyc.cucumber;

import co.za.kyc.KnowYourCustomerApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = KnowYourCustomerApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
