package co.za.kyc.domain.enumeration;

/**
 * The STATUS enumeration.
 */
public enum STATUS {
    Y,
    N,
}
