/**
 * View Models used by Spring MVC REST controllers.
 */
package co.za.kyc.web.rest.vm;
